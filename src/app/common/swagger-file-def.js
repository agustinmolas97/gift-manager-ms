const swaggerJSDoc = require('swagger-jsdoc');
const swaggerParser = require('swagger-parser');
const j2s = require('joi-to-swagger');
const config = require('config')
const logger = require('./logger');
const packageInfo = require('../../../package.json');
const { generatedJoiSchemas } = require('./create-joi-swagger-schema');
const msTitle = config.get('ms-title');

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: msTitle,
    version: packageInfo.version,
    description: packageInfo.description,
  },
};

const generateDoc = (apis) => {
  const swaggerSpec = swaggerJSDoc({
    swaggerDefinition,
    apis,
  });

  const { components: joiSchemasToSwaggerComponents } = j2s(generatedJoiSchemas);
  // merge Joi schemas with swagger definition
  
  swaggerSpec.components.schemas = {
    ...swaggerSpec.components.schemas,
    ...joiSchemasToSwaggerComponents.schemas,
  };

  delete swaggerSpec.channels;

  swaggerParser.validate(swaggerSpec, (err, api) => {
    if (err) {
      logger.error(err.message);
    } else {
      logger.info('Swagger is valid', { message: 'Swagger Valid!' });
    }
  });
  return swaggerSpec;
};

module.exports = { generateDoc };