const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const logger = require('./logger');
const { errorMapper } = require('../mappers');

const tokenSecret = process.env.TOKEN_SECRET;

const generateJWT = (dataToSign)=> {
    const token = jwt.sign(dataToSign, tokenSecret);
    return token;
}

const hashPassword = async (password) => {
    try{
        const salt = await bcrypt.genSalt(10)
        const hashedPassword = await bcrypt.hash(password,salt);
        return hashedPassword
    } catch(error){
        logger.error('An error has ocurred while hashing password: %O', errorMapper(error));
        throw error;
    }
}

const comparePasswords = async(inputPassword, storedPassword) => {
    try{
        const passwordsMatch = await bcrypt.compare(inputPassword, storedPassword)
        return passwordsMatch;
    } catch(error){
        logger.error('An error has ocurred while comparing password: %O', errorMapper(error));
        throw error;
    }
}

module.exports = {
    generateJWT,
    hashPassword,
    comparePasswords
}