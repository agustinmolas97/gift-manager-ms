const joi = require('joi');
const logger = require('./logger');

const generatedJoiSchemas = {};

/**
 * Creates a new Joi object that will be transformed to a new swagger schema
 * @param {String} className the Schema keyname
 * @param {Object} schema an plain javascript Object containing Joi elements
 * @example
 * const User = { id: joi.string()}
 * swaggerSchema('User', user)
 * // can be referenced as "#components/schemas/User"
 */

const createSchema = (className, schema) => {
  try {
    Object.keys(schema).forEach((key) => {
      if (!joi.isSchema(schema[key])) {
        throw new Error(`Property ${key} must be an instance of Joi`);
      }
    });
    const joiSchema = joi.object().keys(schema).meta({ className });
    generatedJoiSchemas[className] = joiSchema;
    return joiSchema;
  } catch (error) {
    logger.error(error.message);
    throw error;
  }
};

module.exports = { createSchema, generatedJoiSchemas };
