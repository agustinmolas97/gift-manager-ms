const config = require('config');
const logger = require('./common/logger');
const packageInfo = require('../../package.json');

// Get from config
const timeout = config.get('kill-timeout');

// On server internal error.
const onServerError = () => {
  logger.info('SERVER ERROR');
};

// On server start.
const onListen = (port) => {
  logger.info(`Application ${packageInfo.name} started at port ${port}`, {
    type: 'application_start',
    applicationName: packageInfo.name,
    port,
    env: process.env.NODE_ENV,
  });
};

// When the process receive kill signal.
const onProcessKill = (server) => () => {
  logger.info('Service termination signal received');
  return new Promise((resolve) => {
    setTimeout(() => {
      logger.info('Finishing server');
      return server.close(() => process.exit(0));
    }, timeout).then(resolve());
  });
};

// When in the server happen a uncaugth exception.
const onException = (err) => {
  logger.error(err);
};

module.exports = {
  onListen,
  onProcessKill,
  onServerError,
  onException,
};
