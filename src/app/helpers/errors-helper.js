const { StatusCodes } = require('http-status-codes');

  const handleError = (error, res) => {
    const { statusCode, message, reason = '' } = error;
  
    res.status(statusCode || StatusCodes.INTERNAL_SERVER_ERROR).send({
      statusCode,
      message,
      reason
    });
  };

module.exports = {
  handleError
}