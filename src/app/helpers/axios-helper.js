const axios = require('axios');
const url = require('url');
const config = require('config');
const { StatusCodes } = require('http-status-codes');
const logger = require('../common/logger');
const { errorMapper } = require('../mappers');

const request = async (
  path,
  options = { method: 'GET', timeout: 3000, throwOnNotFound: true },
  { headers = {}, service = 'meli'} = {},
  { local = false, port = 3001 } = {}
) => {
  const host = url.format(config.get(`services.${service}.url`));
  try {
    const { data = null } = await axios.request({
      url: `${local ? `http://localhost:${port}` : host}/${path}`,
      headers,
      ...options
    });
    return data;
  } catch (error) {
    if (
      !options.throwOnNotFound &&
      (error.response?.status === StatusCodes.NOT_FOUND ||
        error.status === StatusCodes.NOT_FOUND)
    ) {
      logger.error(`Error while ${options.method} data on ${service}-service:${error.message}: %O`,
        errorMapper(error)
      );
      return null;
    } else {
      logger.error(`Error detected at api helper in ${config.get('ms-title')}`);
      logger.error(
        `Error while ${options.method} data on ${service}: %O`, 
        errorMapper(error)
      );
      throw error;
    }
  }
};

module.exports = {
  request
};