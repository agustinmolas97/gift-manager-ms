const logger  = require('../common/logger')
const { StatusCodes } = require('http-status-codes');
const { handleError } = require('../helpers');

const joiValidationError = (err, _, res, next) => {
  if (err.error && err.error.isJoi && err.error.details) {
    const validationMessage = err.error.details
      .map((detail) => `${detail.message}`)
      .join(' | ');

    logger.error(validationMessage);

    return handleError(
      {
        statusCode: StatusCodes.BAD_REQUEST,
        message: 'Bad request',
        reason: validationMessage
      },
      res,
    );
  }
  return next(err);
};

module.exports = { joiValidationError };
