const assert = require('assert');

const errorHandler = (loggerFuncion) => (err, req, res, next) => {
  assert(loggerFuncion);
  assert(err);
  assert(res);

  loggerFuncion(err);
  const error = {
    status : (err.status) ? err.status  : 500,
    message: (err.message && err.status != 500) ? err.message : 'Internal Server Error'  
  };
 
  res.status(error.status).json(error);
};

module.exports = {
  errorHandler
};
