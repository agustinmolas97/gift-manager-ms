
const { json: expressJson } = require('express');
const { errorHandler } = require('./error-handler');
const entryRoutes = require('../api-services/entry-routes');
const { applySwaggerConfig } = require('./swagger');
const logger = require('../common/logger');
const { joiValidationError } = require('./joi-validation-error');
const { morganMiddleware } = require('./morgan-logger');


const applyMiddlewares = (app) => {
    applySwaggerConfig(app);
    app.use(expressJson());
    app.use(morganMiddleware);
    app.use(entryRoutes);
    app.use(joiValidationError);
    app.use(errorHandler(logger.error));
}

module.exports = applyMiddlewares;