const swaggerUi = require('swagger-ui-express');
const { generateDoc } = require('../common/swagger-file-def');

const environment = process.env.NODE_ENV;

const applySwaggerConfig = (app) => {
    if (environment !== 'production') {
        const swaggerDoc = generateDoc([
            './src/app/api-services/**/*-routes*.js',
            './src/app/api-services/**/*-swagger*.js',
          ]);

          app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
          app.use('/api-docs.json', (req, res) => res.json(swaggerDoc));
          return app;
    }
    return null
}

module.exports = {
    applySwaggerConfig
}
