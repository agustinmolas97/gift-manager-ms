const jwt = require('jsonwebtoken');
const logger = require('../common/logger');
const { errorMapper } = require('../mappers');

const verifyToken = (req, res, next) => {
    const bearerHeader = req.headers['authorization'];
    const tokenSecret = process.env.TOKEN_SECRET;
    if(typeof bearerHeader !== 'undefined' && bearerHeader.startsWith('Bearer ')){
        try{
            const bearerToken = bearerHeader.split(" ")[1];
            const verified = jwt.verify(bearerToken, tokenSecret);
            req.user = verified;
            return next();
        }catch(error){
            logger.error('Invalid token: %O', errorMapper(error));
            return res.status(400).send({error: 'Invalid Token'});
        }
    }
    return res.status(400).send({error: 'Bearer authorization header has invalid format'});
}

module.exports = {
    verifyToken
}