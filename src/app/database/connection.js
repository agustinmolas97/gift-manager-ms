const mongoose = require('mongoose');
const logger = require('../common/logger');
const { errorMapper } = require('../mappers');

//Set up default mongoose connection
const connectToDatabase = () => {
    const dbUser = process.env.MONGODB_USER;
    const dbPassword = process.env.MONGODB_USER_PASSWORD;
    const dbName = process.env.MONGODB_NAME
    const mongoDB = `mongodb+srv://${dbUser}:${dbPassword}@giftmanagercluster.egde9.mongodb.net/${dbName}?retryWrites=true&w=majority`;
    const options = {useNewUrlParser: true, useUnifiedTopology: true};
    mongoose.connect(mongoDB, options)
        .then(() => logger.info('🟢 Successfully connected to database!'))
        .catch(error => logger.error('🔴 Database connection failed: %O', errorMapper(error)));
        const db = mongoose.connection;
    return db;
}

module.exports = {
    connectToDatabase
}