const identifyProductId = (id) => {
    const productId = id.match(/MLA\d+/m)
    return productId && productId[0];
}

module.exports = {
    identifyProductId
}