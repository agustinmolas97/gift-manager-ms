const errorMapper = (error) => ({
    error: {
      message: error.message,
      data: error.response?.data,
      path: error.config?.path,
      stack: error.stack
    }
  });

module.exports = {
    errorMapper
}