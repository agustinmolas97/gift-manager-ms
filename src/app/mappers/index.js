const identifyProductMapper = require('./identify-product');
const errorMapper = require('./error')

module.exports = {
    ...identifyProductMapper,
    ...errorMapper
}