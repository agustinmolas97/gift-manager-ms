const createError = require('http-errors');
const { StatusCodes } = require('http-status-codes');
const { errorMapper } = require('../mappers');
const { request } = require('../helpers');
const logger = require('../common/logger');

const service = 'meli';

const getMeliToken = () => process.env.MELI_APP_TOKEN


const getProduct = async (productId) => {
  try {
    const token = getMeliToken()
    const headers = {
        Authorization: `Bearer ${token}`
    }
    const result = await request(
      `products/${productId}`,
      { method: 'GET' },
      { headers, service },
    );
    return result;
  } catch (error) {
    logger.error('Error getting product data: %O', errorMapper(error));
    throw new createError.BadGateway();
  }
};

module.exports = {
    getProduct,
};
