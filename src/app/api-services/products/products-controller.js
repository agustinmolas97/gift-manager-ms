const logger = require('../../common/logger');
const {
    meliService
} = require('../../external-services');
const { 
    identifyProductId, errorMapper
} = require('../../mappers');
const User = require('../../models/db-schemas/user');

const getProduct = async(req,res,next) => {
    try{
        const { id: productId } = req.query;
        const productIdFormatted = identifyProductId(productId);
        const productData = await meliService.getProduct(productIdFormatted);
        if(!productData) {
          return res.status(404).send({
            error: 'NOT_FOUND',
            message: 'Product not found'
          });
        }
        return res.status(200).send(productData);
    } catch(error) {
        logger.error('Found Error in getProduct Controller: %O', errorMapper(error))
        next(error);
    }
}

const insertProductIdInWishlist = async(req,res,next) => {
  try{
      const { productId } = req.body
      const { id } = req.user;
      const userd = await User.findById(id);
      await userd.wishlist.push(productId);
      await userd.save();
      return res.status(201).send({
        message: 'Product successfully stored!'
      });
  } catch(error) {
      logger.error('Found Error in getProduct Controller: %O', errorMapper(error))
      next(error);
  }
}

const removeProductFromWishlist = async(req,res,next) => {
  try{
    const { productId } = req.body
    const { id } = req.user;
    const user = await User.findById(id);
    const s = await user.wishlist.pull(productId);
    await user.save();
    return res.status(201).send({
      message: 'Product successfully removed from your wishlist!'
    });
  } catch(error) {
      logger.error('Found Error in getProduct Controller: %O', errorMapper(error))
      next(error);
  }
}


module.exports = {
  getProduct,
  insertProductIdInWishlist,
  removeProductFromWishlist
};
