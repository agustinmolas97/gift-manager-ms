/**
 * @swagger
 *
 * /v1/product:
 *  get:
 *    summary: Retrieves data from single product found on MELI API
 *    security: 
 *      - bearerAuth: []
 *    tags:
 *      - products
 *    parameters:
 *      - in: query
 *        description: product data
 *        name: url
 *        schema:
 *          $ref: '#/components/schemas/GetProductParams'
 *    responses:
 *      '200':
 *        description: OK - An object with the status success and info from requested product
 */

/**
 * @swagger
 *
 * /v1/product:
 *  patch:
 *    summary: Retrieves data from single product found on MELI API
 *    security: 
 *      - bearerAuth: []
 *    tags:
 *      - products
 *    requestBody:
 *      description: MELI product ID
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/GetProductParams'
 *    responses:
 *      '200':
 *        description: OK - An object with the status success and info from requested product
 */

/**
 * @swagger
 *
 * /v1/product:
 *  delete:
 *    summary: Retrieves data from single product found on MELI API
 *    security: 
 *      - bearerAuth: []
 *    tags:
 *      - products
 *    requestBody:
 *      description: MELI product ID
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/GetProductParams'
 *    responses:
 *      '200':
 *        description: OK - An object with the status success and info from requested product
 */

 module.exports = {};
