const express = require('express');
const { verifyToken } = require('../../middlewares/verify-token');
const joiValidator = require('express-joi-validation').createValidator({
  passError: true,
});
const {
  getProductParamsSchema,
} = require('../../models/swagger-schemas');
const productsController = require('./products-controller');

const router = express.Router();

router
  .route('/product')
  .get(
    joiValidator.query(getProductParamsSchema),
    verifyToken,
    productsController.getProduct,
  );

router
  .route('/product')
  .patch(
    joiValidator.query(getProductParamsSchema),
    verifyToken,
    productsController.insertProductIdInWishlist,
  );

router
  .route('/product')
  .delete(
    joiValidator.query(getProductParamsSchema),
    verifyToken,
    productsController.removeProductFromWishlist,
  );

module.exports = router;