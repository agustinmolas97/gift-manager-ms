const express = require('express');
const productsRoutes = require('./products/products-routes');
const userRoutes = require('./user/user-routes');

const router = express.Router();

router.use('/v1', productsRoutes);
router.use('/v1/user', userRoutes);

module.exports = router;