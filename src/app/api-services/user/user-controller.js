const logger = require('../../common/logger');
const { StatusCodes } = require('http-status-codes');
const assert = require('assert');
const { generateJWT, hashPassword, comparePasswords } = require('../../common/auth-manager');
const User = require('../../models/db-schemas/user');


const loginUser = async(req,res,next) => {
    try{
        const {email, password} = req.body;
        const user = await User.findOne({email});
        if(!user){
            logger.error('User does not exists');
            return res.status(400).send({
                message: 'Invalid Credentials'
            });
        }
        const hashedPassword = await hashPassword(password);
        const passwordsMatch = comparePasswords(hashedPassword, user.password);
        if(!passwordsMatch) {
            logger.error('Invalid Password');
            return res.status(400).send({
                message: 'Invalid Credentials'
            });
        }
        const accessToken = await generateJWT({
            name: user.name,
            id: user._id
        });
        return res.status(200).send({
            message: 'Welcome!',
            accessToken
        });
    } catch(error) {
        logger.error('Found Error in getProduct Controller: %O', {
            errorInfo: error
        })
        next(error);
    }
}

const registerUser = async(req,res,next) => {
    try{
        const { name, email, password } = req.body;
        const emailExists = await User.findOne({email});
        if(emailExists){
            logger.error('Email is already registered');
            return res.status(400).send({
                message: 'email already exists'
            });
        }
        const hashedPassword = await hashPassword(password);
        const user = new User({ name, email, password: hashedPassword });
        const userStored = await user.save();
        return res.status(201).send(userStored);
    } catch(error) {
        logger.error('Found Error in getProduct Controller: %O', {
            errorInfo: error
        })
        next(error);
    }
}



module.exports = {
    loginUser,
    registerUser
};
