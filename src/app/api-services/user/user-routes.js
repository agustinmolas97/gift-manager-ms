const express = require('express');
const joiValidator = require('express-joi-validation').createValidator({
  passError: true,
});
const {
  postUserBodySchema,
  postUserBodyLoginSchema
} = require('../../models/swagger-schemas');
const userController = require('./user-controller');

const router = express.Router();

router
  .route('/login')
  .post(
    joiValidator.body(postUserBodyLoginSchema),
    userController.loginUser,
  );

router
  .route('/register')
  .post(
    joiValidator.body(postUserBodySchema),
    userController.registerUser,
  );


module.exports = router;