/**
 * @swagger
 *
 * /v1/user/register:
 *  post:
 *    summary: Add user to database
 *    tags:
 *      - users
 *    requestBody:
 *      description: product data
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/PostUserBody'
 *    responses:
 *      '200':
 *        description: OK - An object with user stored info
 */

/**
 * @swagger
 *
 * /v1/user/login:
 *  post:
 *    summary: Logins User to app
 *    tags:
 *      - users
 *    requestBody:
 *      description: product data
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/PostUserBodyLogin'
 *    responses:
 *      '200':
 *        description: OK - An object with user stored info
 */

 module.exports = {};
