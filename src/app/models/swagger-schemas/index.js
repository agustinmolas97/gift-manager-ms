const productSchema = require('./product-schema');
const userSchema = require('./user-schema');

module.exports = {
  ...productSchema,
  ...userSchema
};
