const joi = require('joi');
const { createSchema } = require('../../common/create-joi-swagger-schema');

const getProductParamsSchema = createSchema('GetProductParams', {
    id: joi
      .string()
      .regex(/MLA\d+/m)
      .required()
      .messages({
        'string.base': 'url should be a string and being well formated',
      })
});
  
module.exports = {
  getProductParamsSchema
};