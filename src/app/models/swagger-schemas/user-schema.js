const joi = require('joi');
const { createSchema } = require('../../common/create-joi-swagger-schema');

const postUserBodySchema = createSchema('PostUserBody', {
    name: joi
      .string()
      .min(6)
      .max(255)
      .required()
      .messages({
        'string.base': 'name should be a string',
      }),
    email: joi
    .string()
    .min(6)
    .max(255)
    .required()
    .email()
    .messages({
      'string.base': 'email should be a string',
    }),
    password: joi
    .string()
    .min(6)
    .max(1024)
    .required()
    .messages({
      'string.base': 'password should be a string',
    })
});

const postUserBodyLoginSchema = createSchema('PostUserBodyLogin', {
    email: joi
    .string()
    .min(6)
    .max(255)
    .required()
    .email()
    .messages({
      'string.base': 'email should be a string',
    }),
    password: joi
    .string()
    .min(6)
    .max(1024)
    .required()
    .messages({
      'string.base': 'password should be a string',
    })
});
  
module.exports = {
    postUserBodySchema,
    postUserBodyLoginSchema
};