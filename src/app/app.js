require('dotenv').config()
const express = require('express');
const config = require('config');
const app = express();
const events = require('./events');
const applyMiddlewares = require('./middlewares/apply');
const { connectToDatabase } = require('./database/connection');

const port = config.get('port') || 3000;
const server = app.listen(port, () => events.onListen(port));


console.log('FRENOS')

applyMiddlewares(app);
connectToDatabase()

// define server event to handle or unhandle processes.
server.on('error', events.onServerError);
process.on('SIGINT', events.onProcessKill(server));
process.on('SIGTERM', events.onProcessKill(server));
process.on('unhandledRejection', events.onException);
process.on('uncaughtException', events.onException);

module.exports = server;