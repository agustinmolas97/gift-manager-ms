const chai = require('chai');
const expect = chai.expect;

const { identifyProductId } = require('../../app/mappers');

describe('identify-product', () => {
    context('test identifyProductId function', () => {
        it('should be able to pass URL and return product id', () => {
            const urlId = 'https://www.mercadolibre.com.ar/teclado-gamer-bluetooth-keychron-k6-qwerty-gateron-blue-ingles-us-color-negro-con-luz-rgb/p/MLA17352288?pdp_filters=category:MLA418448#searchVariation=MLA17352288&position=1&search_layout=stack&type=product&tracking_id=68e50301-9bbc-417c-bc8c-3d9a3c7508b7'
            expect(identifyProductId(urlId)).to.equal('MLA17352288')
        })

        it('should be able to pass only id and return product id', () => {
            const id = 'MLA17352288'
            expect(identifyProductId(id)).to.equal('MLA17352288')
        })

        it('should return null if no id was given', () => {
            const id = ''
            expect(identifyProductId(id)).to.be.null
        })
        
    })
})