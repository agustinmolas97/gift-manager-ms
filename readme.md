# GIFT-MANAGER-MS
A service intended to handle CRUD requests that allows you to register and login to our app, get a product data using MERCADO LIBRE's API and save or remove the product retrieved on your whishlist.

## Installation

Use the node package manager (npm) to install its dependencies

```bash
npm install
```

## Usage

To initialize the project, simply run
```npm
npm start
```
## Documentation

To see our documentation, once the project is running, go to the following URL

* GUI SWAGGER DOC: http://<host>:<port>/api-docs
* JSON COLLECTION: http://<host>:<port>/api-docs.json

It will only be accesible on development mode, please note that by default **host** is localhost and **port** is 3000

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.